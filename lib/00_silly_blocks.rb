def reverse_words_in_array(words)
  words.map { |word| word.reverse }
end

def reverser
  words = yield
  reverse_words_in_array(words.split(' ')).join(' ')
end

def adder(num = 1)
  yield + num
end

def repeater(number = 1)
  (0...number).each { yield }
end
