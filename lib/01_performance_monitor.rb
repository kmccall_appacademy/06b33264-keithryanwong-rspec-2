require "time"

def measure(num=1)
  times = []
  (0...num).each do
    start_time = Time.now
    yield
    end_time = Time.now
    times.push(end_time - start_time)
  end
  times.sum / num
end

class Array
  def sum
    total = 0;
    self.each { |element| total += element}
    total
  end
end
